#!/bin/bash

# custom (non-ppa) installations
# launched automatically from runcmd
# $pwd is clef-agreg/

# SYSTEM CONFIG

# Python packages
python3 -m pip install -r requirements.txt
pip freeze -r requirements.txt > /home/candidat/packages.txt

# Enable phpmymadmin
#ln -s /usr/share/phpmyadmin/ /var/www/html
#mysql -e "create user candidat identified by 'concours'"
#mysql -e "grant all privileges on *.* to candidat with grant option"
#mysql -e "flush privileges"

# Network bugs + conserver le fichier pour rétablir le réseau sur l'iso ?
#mv /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf.bak
#echo "" > /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf

# Install Filius
# wget https://www.lernsoftware-filius.de/downloads/Setup/filius_1.13.2_all.deb
# apt install ./filius_1.13.2_all.deb

# Install ns-3
# wget https://www.nsnam.org/releases/ns-allinone-3.35.tar.bz2
# tar -xf ns-allinone-3.35.tar.bz2
# rm ns-allinone-3.35.tar.bz2
# mv ns-allinone-3.35 /opt/

# Scratch
# wget http://www.ac-grenoble.fr/maths/scratch/scratch.zip
# unzip scratch.zip -d /home/candidat/scratch

# PyCharm
snap install pycharm-community --classic

# Locale
localectl set-locale fr_FR.UTF-8
localectl set-x11-keymap fr

# Cleanup
apt autoclean

# USER CONFIG
# VSCode directories and clipit annoying startup question
mkdir -p /home/candidat/.config/VSCodium
mkdir -p /home/candidat/.vscode-oss/extensions
cp -r config/* /home/candidat/.config
# Tuareg mode
mkdir -p /home/candidat/.emacs.d && cd /home/candidat/.emacs.d && git clone https://github.com/ocaml/tuareg && cd tuareg && make
echo '(load "/home/candidat/.emacs.d/tuareg/tuareg-site-file")' >> /home/candidat/.emacs
chown -R candidat:candidat /home/candidat

su candidat -c "opam init -a"
su candidat -c "opam install -y jupyter utop ocaml-lsp-server"
su candidat -c "eval \$(opam env) && ocaml-jupyter-opam-genspec"
su candidat -c "echo 'eval \$(opam env)' >> /home/candidat/.bashrc"
su candidat -c "echo 'export PATH=/home/candidat/.local/bin:/home/candidat/texlive/bin/x86_64-linux:$PATH' >> /home/candidat/.bashrc"
su candidat -c "jupyter kernelspec install --user --name ocaml-jupyter /home/candidat/.opam/default/share/jupyter"
su candidat -c "codium --install-extension ms-ceintl.vscode-language-pack-fr"
su candidat -c "codium --install-extension ocamllabs.ocaml-platform"
su candidat -c "codium --install-extension ms-python.python"
wget https://github.com/microsoft/vscode-cpptools/releases/download/v1.14.3/cpptools-linux.vsix
su candidat -c "codium --install-extension cpptools-linux.vsix"

# Zeal docsets
mkdir -p /home/candidat/.local/share/Zeal/Zeal/docsets/
cd /home/candidat/.local/share/Zeal/Zeal/docsets/
export LISTE_DOCSETS="Bash C C++ LaTeX Matplotlib NumPy OCaml Python_3 SQLite SciPy"
for DOCSET in ${LISTE_DOCSETS};
    do wget http://paris.kapeli.com/feeds/${DOCSET}.tgz
    tar -zxvf ${DOCSET}.tgz;
    done
rm *.tgz
chown -R candidat:candidat /home/candidat/.local/share/Zeal

rm -rf /var/lib/snapd/cache  # Save 2 GB
rm -rf /var/cache/apt/archives  # Save 1 GB